About
=====

This contains network datasets and source code of programs to process
the networks.  Below are some explanation about the content:

* data/arxiv -- The arXiv dataset, spanning 1986 up to the end of 2011.
  The data was harvested from http://arxiv.org
* data/dimes -- The DIMES dataset on autonomous systems.  It includes
  monthly snapshots from January 2007 up to the end of October 2011.
  It was downloaded from http://www.netdimes.org
* data/gp -- The GP bibliography, spanning from 1950 up to the end of
  2011.  It was downloaded from http://www.cs.bham.ac.uk/~wbl/biblio/
* data/katrina -- The Katrina dataset, spanning from 13:19 hours on
  2005-08-28 to the end of 22:32 hours on 2005-08-31.  The data was
  provided by Jeffrey Chan from The University of Melbourne, Australia.
* src -- Source code of programs we used to process the datasets.
* demo -- This is where the code demontration lives.  It contains
  examples on how to use the packaged software.


Download
========

To download the source code and the datasets, see under the tab
``Downloads`` above.

.. warning::

    The complete collection is 93 MB compressed, but is 514 MB
    uncompressed.  The collection was compressed using lzma in order
    to achieve a very high compression ratio.
