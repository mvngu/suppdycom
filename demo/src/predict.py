###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Compute the changes throughout the history of a temporal group, summarize
# the changes as a H value, and use the H value to make a prediction about
# the future changes of the temporal group.

import argparse
import math
import os

###################################
# helper functions
###################################

def change_community_level(fsindex, nnext, lastsi, hval, comdir, tracedir,
                           outdir):
    """
    Change at the level of dynamic communities.

    INPUT:

    - fsindex -- a file listing all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    - nnext -- how far ahead do we want to predict.
    - lastsi -- the last snapshot index to consider.  This is the latest we
      will consider.  We do not consider any data beyond this snapshot index.
    - hval -- the H value to be used as a threshold for prediction.
    - comdir -- directory where communities are stored.
    - tracedir -- directory where community traces are stored.
    - outdir -- directory where we will write data on anomaly.
    """
    Hmin = float(hval)
    Sindex = read_sindex(fsindex)
    Com = read_community(Sindex, comdir)
    T = list()
    for ell in range(2, len(Sindex) + 1):
        read_trace(tracedir, ell, T)

    dthreshold = 0
    ActualMinus = 0
    # VerifyMinus[0] := how many predictions we make
    # VerifyMinus[1] := how many predictions turn out to be true
    VerifyMinus = [0, 0]
    ActualPlusUniq = 0
    # VerifyPlusUniq[0] := how many predictions we make
    # VerifyPlusUniq[1] := how many predictions turn out to be true
    VerifyPlusUniq = [0, 0]
    for i in range(len(T)):
        # Only retain dynamic communities that satisfy the following
        # conditions:
        #
        # - the H-value up to this point is >= H
        # - the percentage change since the previous snapshot is >= p
        #
        # If a dynamic community satisfies the above two criteria, then we
        # further predict whether it changes in the next snapshot.
        trace = T[i]

        # predict that unique new members will join
        History, delta, deltanext = history_unique_new_members(
            Com, trace, lastsi, nnext)
        if len(History) == 0:
            continue
        assert len(History) > 0
        # get the number of actual changes
        if deltanext > dthreshold:
            ActualPlusUniq += 1
        H = entropy(History)
        # make one prediction and verify it
        if H >= Hmin:
            if delta > dthreshold:
                VerifyPlusUniq[0] += 1
                VerifyPlusUniq[1] += verify_prediction(deltanext, dthreshold)

        # predict that members will leave
        History, delta, deltanext = history_members_depart(
            Com, trace, lastsi, nnext)
        assert len(History) > 0
        # get the number of actual changes
        if deltanext > dthreshold:
            ActualMinus += 1
        H = entropy(History)
        # make one prediction and verify it
        if H >= Hmin:
            if delta > dthreshold:
                VerifyMinus[0] += 1
                VerifyMinus[1] += verify_prediction(deltanext, dthreshold)

    # Write results to files.  If a file exists, just append results to it.
    # The file format is as follows:
    #
    # * the value of the H threshold
    # * how many dynamic communities actually changed by this threshold
    # * how many predictions we make
    # * how many of those predictions turn out to be true
    header = "Hthreshold,#actual,#prediction,#positive\n"
    # members depart
    fname = os.path.join(outdir, "minus-%d.dat" % nnext)
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write(header)
        f.close()
    f = open(fname, "a")
    f.write("%s,%d,%d,%d\n" % (
            hval, ActualMinus, VerifyMinus[0], VerifyMinus[1]))
    f.close()
    # unique new members
    fname = os.path.join(outdir, "plus-unique-%d.dat" % nnext)
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write(header)
        f.close()
    f = open(fname, "a")
    f.write("%s,%d,%d,%d\n" % (
            hval, ActualPlusUniq, VerifyPlusUniq[0], VerifyPlusUniq[1]))
    f.close()

def entropy(History):
    """
    The entropy of the history of some type of changes.

    INPUT:

    - History -- a list giving the sequence of changes with respect to some
      event.  For example, suppose we are interested in the number of new
      nodes joining a dynamic community.  Then History[i] represents the
      required count in snapshot i.
    """
    S = float(math.fsum(History))
    if S == 0.0:
        return 0.0
    assert S > 0.0
    P = [e / S for e in History]
    H = list()
    for p in P:
        if p == 0.0:
            H.append(0.0)
        else:
            H.append(p * math.log(p))  # natural logarithm
    H = -1.0 * math.fsum(H)
    if H == 0.0:
        H = 0.0
    return H

def history_members_depart(Com, trace, lastsi, nnext):
    """
    The history of members departing the given dynamic community D.  Let i and
    j be consecutive snapshots, where i < j.  Let N_i and N_j be the set of
    nodes at snapshots i and j, respectively.  All the nodes from N_i that
    leave N_j can be obtained by the set difference N_i - N_j.  That is, we
    consider all nodes that are in N_i but not in N_j.

    INPUT:

    - Com -- a dictionary of all communities in all snapshots.  We use the key
      "y i" to refer to community i in snapshot y.
    - trace -- the trace of a dynamic community.  The trace is represented as
      (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk).  Each 2-tuple
      is considered for departing members.
    - lastsi -- the last snapshot index to consider.  This is the latest we
      will consider.  We do not consider any data beyond this snapshot index.
    - nnext -- how far ahead do we want to predict.
    """
    # the history to be used for prediction
    History = list()
    m = 0  # no. snapshots with missing observations
    d = 0  # how many nodes leave the dynamic community
    A = None
    # Do we want to use this dynamic community?  We only want to use a
    # dynamic community if its trace includes the snapshot index we want.
    # For example, suppose we want the history up to snapshot index si.  If
    # the history of a dynamic community does not cover this snapshot index,
    # then we do not use the dynamic community.
    usedycom = False
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # have we processed changes for the latest snapshot index?
        if sia >= lastsi:
            usedycom = True
            break
        assert sia < lastsi
        assert sib <= lastsi
        # are we missing observations?
        if sia == sib:
            m += 1
        else:
            m = 0
        # get the required changes
        A = Com[a]
        B = Com[b]
        d = len(A.difference(B))
        History.append(d)
        if (sia == sib) and (sia + m == lastsi):
            usedycom = True
            break
    # We have no history if the dynamic community is born on or after the
    # snapshot index we want.
    if len(History) == 0:
        return list(), 0, 0
    # We do not use a dynamic community if its history does not cover the
    # snapshot index we want.  In particular, we refuse to use the dynamic
    # community if it dies before the snapshot index under consideration.
    if not usedycom:
        return list(), 0, 0
    delta = 0
    if A is not None:
        delta = int(d)

    # the history to be used for verification
    m = 0  # no. snapshots with missing observations
    d = 0  # how many nodes leave the dynamic community
    A = None
    # Do we want to use this dynamic community?  We only want to use a
    # dynamic community if its trace includes the snapshot index we want.
    # For example, suppose we want the history up to snapshot index si.  If
    # the history of a dynamic community does not cover this snapshot index,
    # then we do not use the dynamic community.
    usedycom = False
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # have we processed changes for the latest snapshot index + nnext?
        if sia >= lastsi + nnext:
            usedycom = True
            break
        assert sia < lastsi + nnext
        assert sib <= lastsi + nnext
        # are we missing observations?
        if sia == sib:
            m += 1
        else:
            m = 0
        # get the required changes
        A = Com[a]
        B = Com[b]
        d = len(A.difference(B))
        if (sia == sib) and (sia + m == lastsi + nnext):
            usedycom = True
            break
    deltanext = 0
    if usedycom and (A is not None):
        deltanext = int(d)

    return History, delta, deltanext

def history_unique_new_members(Com, trace, lastsi, nnext):
    """
    The history of unique new members in a given dynamic community D.  Let i
    and j be consecutive snapshots, where i < j.  Let N_i and N_j be the set
    of nodes at snapshots i and j, respectively.  All the new nodes that join
    the network at snapshot j can be obtained by the set difference N_j - N_i.
    That is, we consider all nodes that are in N_j but not in N_i.
    Furthermore, any such nodes must not appear in D prior to snapshot j.

    INPUT:

    - Com -- a dictionary of all communities in all snapshots.  We use the key
      "y i" to refer to community i in snapshot y.
    - trace -- the trace of a dynamic community.  The trace is represented as
      (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk).  Each 2-tuple
      is considered for new members.
    - Node -- all members of dynamic community D, up to and including
      snapshot i.
    - lastsi -- the last snapshot index to consider.  This is the latest we
      will consider.  We do not consider any data beyond this snapshot index.
    - nnext -- how far ahead do we want to predict.
    """
    # the history to be used for prediction
    # All nodes in first snapshot of dynamic community.  We update this as
    # new and unique nodes join the dynamic community.  A node might leave D
    # in one snapshot and subsequently rejoin D at a later snapshot.  We are
    # only interested in the first time that a node joins D.
    Node = dict()
    a, _ = trace[0]
    [Node.setdefault(v, True) for v in Com[a]]
    # history of unique members
    History = list()
    m = 0  # no. snapshots with missing observations
    d = 0  # how many unique new nodes join the dynamic community
    A = None
    # Do we want to use this dynamic community?  We only want to use a
    # dynamic community if its trace includes the snapshot index we want.
    # For example, suppose we want the history up to snapshot index si.  If
    # the history of a dynamic community does not cover this snapshot index,
    # then we do not use the dynamic community.
    usedycom = False
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # have we processed changes for the latest snapshot index?
        if sia >= lastsi:
            usedycom = True
            break
        assert sia < lastsi
        assert sib <= lastsi
        # are we missing observations?
        if sia == sib:
            m += 1
        else:
            m = 0
        # get the required changes
        A = Com[a]
        B = Com[b]
        i = 0  # how many new unique members?
        for v in B.difference(A):
            if v not in Node:
                i += 1
                Node.setdefault(v, True)
        d = i
        History.append(d)
        if (sia == sib) and (sia + m == lastsi):
            usedycom = True
            break
    # We have no history if the dynamic community is born on or after the
    # snapshot index we want.
    if len(History) == 0:
        return list(), 0, 0
    # We do not use a dynamic community if its history does not cover the
    # snapshot index we want.  In particular, we refuse to use the dynamic
    # community if it dies before the snapshot index under consideration.
    if not usedycom:
        return list(), 0, 0
    delta = 0
    if A is not None:
        delta = int(d)

    # the history to be used for verification
    # All nodes in first snapshot of dynamic community.  We update this as
    # new and unique nodes join the dynamic community.  A node might leave D
    # in one snapshot and subsequently rejoin D at a later snapshot.  We are
    # only interested in the first time that a node joins D.
    Node = dict()
    a, _ = trace[0]
    [Node.setdefault(v, True) for v in Com[a]]
    m = 0  # no. snapshots with missing observations
    d = 0  # how many unique new nodes join the dynamic community
    A = None
    # Do we want to use this dynamic community?  We only want to use a
    # dynamic community if its trace includes the snapshot index we want.
    # For example, suppose we want the history up to snapshot index si.  If
    # the history of a dynamic community does not cover this snapshot index,
    # then we do not use the dynamic community.
    usedycom = False
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # have we processed changes for the latest snapshot index + nnext?
        if sia >= lastsi + nnext:
            usedycom = True
            break
        assert sia < lastsi + nnext
        assert sib <= lastsi + nnext
        # are we missing observations?
        if sia == sib:
            m += 1
        else:
            m = 0
        # get the required changes
        A = Com[a]
        B = Com[b]
        i = 0  # how many new unique members?
        for v in B.difference(A):
            if v not in Node:
                i += 1
                Node.setdefault(v, True)
        d = i
        if (sia == sib) and (sia + m == lastsi + nnext):
            usedycom = True
            break
    deltanext = 0
    if usedycom and (A is not None):
        deltanext = int(d)

    return History, delta, deltanext

def parse_trace(trace):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline of a dynamic
    community.  Each step community is represented as

        si com

    where "si" refers to the snapshot index and "com" refers to the index
    of the community in the given snapshot.

    INPUT:

    - trace -- the trace of a dynamic community.
    """
    T = trace.split("->")
    # transform the pattern "si1 com1,n" into "si1 com1"
    for i in range(len(T)):
        if "," in T[i]:
            sicom, _ = T[i].split(",")
            T[i] = sicom
    return T

def read_community(Sindex, comdir):
    """
    Read in all the communities in each network snapshot.  For a network
    snapshot y, each community in the snapshot is assigned a unique
    nonnegative integer, starting from zero.  Let i be the unique label
    assigned to some community.  We use the key "y i" to refer to community i
    in snapshot y.

    INPUT:

    - Sindex -- a list of all the snapshot indices.
    - comdir -- directory where communities are stored.
    """
    # Dictionary of all communities.  Each key is a string of the form "y i".
    # The value corresponding to the key is the set of all nodes belonging to
    # community "y i".
    D = dict()
    for si in Sindex:
        i = 0
        fname = os.path.join(comdir, "comm-%d.dat" % si)
        if not os.path.isfile(fname):
            continue
        f = open(fname, "r")
        ncom = int(f.readline().strip())  # first line is number of communities
        if ncom < 1:
            f.close()
            continue
        # get the set of nodes belonging to community "y i"
        for line in f:
            S = line.strip().split()
            Node = set(map(int, S))
            assert len(S) == len(Node)
            key = "%d %d" % (si, i)
            D.setdefault(key, Node)
            i += 1
        f.close()
    return D

def read_sindex(fname):
    """
    Get all the snapshot indices from the given file.

    INPUT:

    - fname -- file with all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    """
    Sindex = []
    f = open(fname, "r")
    for line in f:
        Sindex.append(int(line.strip()))
    f.close()
    return Sindex

def read_trace(tracedir, ell, T):
    """
    Read in the trace of all dynamic communities with the given lifespan.
    The trace of a dynamic community is represented as:

        y1 n1->y2 n2->y3 n3-> ... ->yk nk

    After reading in the trace, we represent it as:

        (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk)

    WARNING:  This function modifies its arguments.

    INPUT:

    - tracedir -- directory where community traces are stored.
    - ell -- the lifespan of a dynamic community.  Assumed to be > 1.
    - T -- a list with traces of dynamic communities.  The traces will be
      added to this list.
    """
    if ell < 2:
        raise ValueError("Lifespan must be > 1")
    fname = os.path.join(tracedir, "trace-%d.dat" % ell)
    f = open(fname, "r")
    ncom = int(f.readline().strip())  # how many dynamic communities
    if ncom > 0:
        for line in f:
            n = len(T)
            T.append(list())
            trace = parse_trace(line.strip())
            for i in range(len(trace) - 1):
                T[n].append((trace[i], trace[i + 1]))
    f.close()

def verify_prediction(deltanext, dthreshold):
    """
    Verify that a temporal group actually changes by a given threshold in
    the next time step.

    INPUT:

    - deltanext -- how many changes since the current snapshot.  This
      is computed by looking at the next snapshot.  We use this to verify
      our prediction.
    - dthreshold -- a minimum threshold value to be used to verify our
      prediction.
    """
    # prediction turns out to be true
    if deltanext > dthreshold:
        return 1
    # prediction turns out to be false
    return 0

###################################
# script starts here
###################################

# setup parser for command line options
s = "Predict the future changes of a temporal group.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--fsindex", metavar="file", required=True,
                    help="listing of all snapshot indices")
parser.add_argument("--nnext", metavar="integer", required=True, type=int,
                    help="how far ahead we want to predict")
parser.add_argument("--lastsi", metavar="integer", required=True, type=int,
                    help="the last snapshot index to consider")
parser.add_argument("--h", metavar="string", required=True,
                    help="the H threshold for prediction")
parser.add_argument("--comdir", metavar="path", required=True,
                    help="directory where communities are stored")
parser.add_argument("--tracedir", metavar="path", required=True,
                    help="directory where community traces are stored")
parser.add_argument("--outdir", metavar="path", required=True,
                    help="directory where we will write data on anomaly")
args = parser.parse_args()
# get command line arguments & sanity checks
fsindex = args.fsindex
# How far ahead we want to predict.  This might be one snapshot ahead, two
# snapshots ahead, and so on.
nnext = args.nnext
# The last snapshot index to consider.  This is the latest we will consider.
# We do not consider any data beyond this snapshot index.
lastsi = args.lastsi
# the H value will be used to predict what would happen in the next snapshot
hval = args.h
# This should be the directory where we can find all the communities that
# were used in the matching process.  It is not necessarily the same as the
# directory storing all the communities extracted from all snapshots.
comdir = args.comdir
tracedir = args.tracedir
outdir = args.outdir

if not os.path.isdir(outdir):
    os.makedirs(outdir)
change_community_level(fsindex, nnext, lastsi, hval, comdir, tracedir, outdir)
