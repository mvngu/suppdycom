#!/usr/bin/env bash

###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# A code demonstration.  This should automate the following tasks:
#
# * Create network snapshots.
# * Extract all communities.
# * Get all events in community life-cycle.

# WARNING:  It's very important that you first set the library path.
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/lib/dir
export LD_LIBRARY_PATH
# path to your local installation of Python
Python=/path/to/your/python
# path to your local installation of Sage
Sage=/path/to/your/sage

smin=1     # the minimum community size
theta=0.3  # match threshold
delta=0    # no. allowable missing observations
gamma=0.1  # growth threshold
kappa=0.1  # contraction threshold
# the algorithm to use for community detection
comalgo=blondel
# directory to store the extracted communities
comdir=../data/com"$comalgo"
# a list of snapshot indices, one index per line
fsidx=../data/gp-snapshot-index.dat
# the data file in CSV format
fdata=../data/gp.csv
# directory to store all data for each snapshot index
sidir=../data/sidata
# directory to store all network snapshots
snapshotdir=../data/snapshot

echo "Cleanse dataset"
"$Python" cleanames.py "$fdata"

echo "Create network snapshots"
# get all data for each snapshot index
if [ ! -d "$sidir" ]; then
    mkdir -p "$sidir"
fi
for si in $(< "$fsidx"); do
    grep "$si" "$fdata" > "$sidir"/"$si".dat
    # If we don't have data for the given snapshot index, then we carry
    # over the data from the previous snapshot index.
    if [ "$?" -eq 1 ]; then
        prevsi=$((si - 1))
        cp "$sidir"/"$prevsi".dat "$sidir"/"$si".dat
    fi
done
# now construct a network snapshot for each snapshot index
if [ ! -d "$snapshotdir" ]; then
    mkdir -p "$snapshotdir"
fi
"$Python" snapshot.py \
    --year "$fsidx" \
    --datadir "$sidir" \
    --edgelistdir "$snapshotdir"

# This is where we construct a dynamic network, infer temporal groups, and
# extract events in the life-cycle of a community.
echo "Extract communities"
if [ ! -d "$comdir" ]; then
    mkdir -p "$comdir"
fi
./extractcom \
    --sindex "$fsidx" \
    --edgelistdir "$snapshotdir" \
    --algo "$comalgo" \
    --comdir "$comdir" 2>&1 > /dev/null

echo "Extract all communities with a given minimum size"
"$Python" minsize.py \
    --smin "$smin" \
    --year "$fsidx" \
    --comdir "$comdir" \
    --outdir "$comdir"/event
echo "Match all communities with a given minimum size"
"$Python" match.py \
    --smin "$smin" \
    --theta "$theta" \
    --delta "$delta" \
    --year "$fsidx" \
    --eventdir "$comdir"/event
echo "Get all events, except for death"
"$Python" event.py \
    --deathonly false \
    --smin "$smin" \
    --theta "$theta" \
    --delta "$delta" \
    --gamma "$gamma" \
    --kappa "$kappa" \
    --year "$fsidx" \
    --eventdir "$comdir"/event
echo "Generate the trace network"
"$Sage" -python tracecomm.py \
    --smin "$smin" \
    --theta "$theta" \
    --delta "$delta" \
    --year "$fsidx" \
    --eventdir "$comdir"/event
echo "Get all death events"
"$Python" event.py \
    --deathonly true \
    --smin "$smin" \
    --theta "$theta" \
    --delta "$delta" \
    --gamma "$gamma" \
    --kappa "$kappa" \
    --year "$fsidx" \
    --eventdir "$comdir"/event
