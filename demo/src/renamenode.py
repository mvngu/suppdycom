###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Rename the nodes in an edge list.  This is so we would be able to use
# igraph to process the edge list.

import argparse

###################################
# script starts here
###################################

# setup parser for command line options
s = "Rename the nodes of an edge list.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--infile", metavar="path", required=True,
                    help="read the edge list from this file")
parser.add_argument("--outfile", metavar="path", required=True,
                    help="write the renamed edge list to this file")
args = parser.parse_args()

# get the command line arguments
infile = args.infile
outfile = args.outfile

# read in edge list and rename each unique node
V = dict()  # mapping from old node label to new node label
fin = open(infile, "r")
fout = open(outfile, "w")
for line in fin:
    u, v = line.strip().split()
    if u not in V:
        V.setdefault(u, len(V))
    if v not in V:
        V.setdefault(v, len(V))
    fout.write("%d %d\n" % (V[u], V[v]))
fin.close()
fout.close()
