#!/usr/bin/env bash

###########################################################################
# Copyright (C) 2011 Minh Van Nguyen <nguyenminh2@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Automate the configure, compile and documentation build of igraph.

# pre-configuration
make clean
make distclean
autoreconf --force --install

if [ -e "compile.log" ] && [ -f "compile.log" ]; then
    rm -f compile.log
fi
if [ -e "configure.log" ] && [ -f "configure.log" ]; then
    rm -f configure.log
fi

# configure
if [ `uname -n` = "bsd.local" ]; then
    ./configure --prefix=/Users/mvngu/usr --disable-graphml --disable-gmp --disable-glpk --enable-gcc-warnings 2>&1 | tee -a configure.log
elif [ `uname -n` = "darkstar" ] || [ `uname -n` = "melb" ]; then
    ./configure --prefix=/home/mvngu/usr 2>&1 | tee -a configure.log
elif [ `uname -n` = "geom" ]; then
    ./configure --prefix=/scratch/mvngu/usr --disable-graphml --disable-gmp --disable-glpk | tee -a configure.log
elif [ `uname -n` = "mod.math.washington.edu" ]; then
    ./configure --prefix=/scratch/mvngu/usr --disable-graphml --disable-gmp --disable-glpk | tee -a configure.log
elif [ `uname -n` = "node00.cluster.csse.unimelb.edu.au" ]; then
    ./configure --prefix=/home/pgrad/minguyen/usr --enable-gcc-warnings 2>&1 | tee -a configure.log
elif [ `uname -n` = "redhawk" ]; then
    ./configure --prefix=/dev/shm/mvngu/usr --disable-graphml --disable-gmp --disable-glpk --enable-gcc-warnings 2>&1 | tee -a configure.log
elif [ `uname -n` = "sage.math.washington.edu" ]; then
    ./configure --prefix=/scratch/mvngu/usr --disable-graphml --disable-gmp --disable-glpk --enable-gcc-warnings 2>&1 | tee -a configure.log
fi

# compile and install
make 2>&1 | tee -a compile.log
make install
## make check
if [ `uname -n` = "bsd.local" ]; then
    make check
elif [ `uname -n` = "node00.cluster.csse.unimelb.edu.au" ]; then
    make check
elif [ `uname -n` = "redhawk" ]; then
    make check
elif [ `uname -n` = "sage.math.washington.edu" ]; then
    make check
fi

# build documentation
if [ `uname -n` = "darkstar" ] || [ `uname -n` = "melb" ]; then
    cd doc && make html
fi
