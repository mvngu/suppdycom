<h2>Screenshots</h2>

<a name="1"></a>

<h3>The Fruchterman-Reingold layout algorithm</h3>

<img class="float_right" width="480" height="480" alt="Illustration" src="images/screenshots/frplots.png"/>

<div style="border-right: 490px solid white">
<p>The Fruchterman-Reingold is a robust algorithm to generate 
visually appealing placements for vertices. It works well on unconnected 
graphs and can be used up to a couple of thousand vertices.</p>

<p>In this example we use the igraph R package, and its Cairo device, 
as it can produce nice antialiased pictures. If you don't have the
Cairo package installed, just remove the 
<code>CairoX11()</code> line from the code.
</p>

<p>The <code>frgraphs()</code> function is <a href="frgraphs.R">available
here</a>.</p>

<pre class="programlisting condensed">
library(igraph)
library(Cairo)

graphs &lt;- frgraphs()
lay &lt;- lapply(graphs, layout.fruchterman.reingold, niter=3000)

CairoX11()
# CairoPNG(file="frplots.png")
par(mai=c(0,0,0,0))
layout(matrix(1:16, nr=4, byrow=TRUE))
layout.show(16)
for (i in seq(along=graphs)) {
  plot(graphs[[i]], layout=lay[[i]],
       vertex.label=NA, vertex.size=13, edge.color="black",
       vertex.color="red")
}
# dev.off()
</pre>
</div>

<hr style="clear:both"/>

<a name="2"></a>

<h3 style="clear:both">The <code>tkplot</code> editor of the R package</h3>

<img class="float_right" width="462" height="507" alt="Illustration" src="images/screenshots/tkplot.png"/>
<div style="border-right: 472px solid white">
<p>The igraph R package features an interactive graph layout editor
for small graphs. It is a little toy only, but can be useful if you
want to adjust the layout of small graphs. Here is how we generated 
this picture:</p>
<pre class="programlisting condensed">
library(igraph)
g &lt;- read.graph("karate.net", format="pajek")

cs &lt;- leading.eigenvector.community.step(g)
V(g)$color &lt;- ifelse(cs$membership==0, "lightblue", "green")

scale &lt;- function(v, a, b) {
  v &lt;- v-min(v) ; v &lt;- v/max(v) ; v &lt;- v * (b-a) ; v+a
}

V(g)$size &lt;- scale(abs(cs$eigenvector), 10, 20)
E(g)$color &lt;- "grey"
E(g)[ V(g)[ color=="lightblue" ] %--% V(g)[ color=="green" ] ]$color &lt;- "red"

tkplot(g, layout=layout.kamada.kawai, vertex.label.font=2)
</pre>
<p>After hand-tuning the layout, you can obtain the new coordinates
using the id of the plot, as shown on the window border:</p>
<pre class="programlisting condensed">
coords &lt;- tkplot.getcoords(7)
</pre>
<p>You can find the <a href="karate.net">karate.net file here</a>.</p>
</div>

<hr style="clear:both"/>

<a name="3"></a>
<h3 style="clear:both">Transparency with the Cairo R package</h3>

<img class="float_right" width="497" height="596" alt="Illustration" src="images/screenshots/fastgreedy.png"/>

<div style="border-right: 507px solid white">
<p>The graphics device from the Cairo R package handles 
transparency, we use this to plot one graph on top of another.</p>
<pre class="programlisting condensed">
library(igraph) ; library(Cairo)

g &lt;- barabasi.game(100, power=0.6, m=10)
V(g)$name &lt;- seq(vcount(g))-1
g &lt;- simplify(g)
l &lt;- layout.fruchterman.reingold(g)
l &lt;- layout.norm(l, -1,1, -1,1) 

fcs &lt;- fastgreedy.community(simplify(as.undirected(g)))
Q &lt;- round(max(fcs$modularity), 3)
fcs &lt;- community.to.membership(g, fcs$merges, steps=which.max(fcs$modularity)-1 )

CairoX11() # or CairoPNG(file="fastgreedy.png")

plot(g, layout=l, vertex.size=3, vertex.label=NA, vertex.color="#ff000033",
     vertex.frame.color="#ff000033", edge.color="#55555533", edge.arrow.size=0.3,
     rescale=FALSE, xlim=range(l[,1]), ylim=range(l[,2]),
     main=paste(sep="", "Fast greedy community detection,\nQ=", Q))

g2 &lt;- subgraph(g, which(fcs$membership==fcs$membership[1])-1)
l2 &lt;- l[ which(fcs$membership==fcs$membership[1]), ]

plot(g2, layout=l2, vertex.size=3, vertex.label=V(g2)$name,
     vertex.color="#ff0000", vertex.frame.color="#ff0000", edge.color="#555555",
     vertex.label.dist=0.5, vertex.label.cex=0.8, vertex.label.font=2,
     edge.arrow.size=0.3, add=TRUE, rescale=FALSE)

nodes &lt;- which(fcs$membership==fcs$membership[1])-1
nodes &lt;- paste(collapse=", ", nodes)
text(0,-1.3, nodes, col="blue")

# dev.off()
</pre>
</div>

<hr style="clear:both"/>

<a name="4"></a>

<h3 style="clear:both">Components of an Erd&#x0151;s-R&#x00e9;nyi random graph</h3>

<img class="float_right" width="480" height="480" src="images/screenshots/erdos_renyi_components.png" alt="Illustration"/>
<div style="border-right: 490px solid white">
<p>Erd&#x0151;s-R&#x00e9;nyi random graphs have a fixed number of vertices
(given by the parameter <i>n</i>) and a fixed number of edges (given by
<i>m</i>) that are placed between pairs of vertices with equal probability.
When one increases <i>m</i> gradually, small disconnected tree-like
components will appear, but there exists a threshold <i>m'</i>
(depending on <i>n</i>) where a giant component appears suddenly.</p>
<p>In this visualization, we used the Python interface to show an
Erd&#x0151;s-R&#x00e9;nyi graph near its threshold <i>m'</i>.
Components are colored with different colors according to their size,
isolated vertices are colored with light gray.</p>

<pre class="programlisting condensed">
from igraph import *

g = Graph.Erdos_Renyi(n=300, m=250)
colors = ["lightgray", "cyan", "magenta", "yellow", "blue", "green", "red"]
for component in g.components():
  color = colors[min(6, len(component)-1)]
  for vidx in component: g.vs[vidx]["color"] = color
  
plot(g, layout="fr", vertex_label=None)
</pre>
</div>

<hr style="clear:both"/>

<a name="5"></a>

<h3 style="clear:both">Kautz graph and its adjacency matrix</h3>

<img class="float_right" width="480" height="480" src="images/screenshots/kautz.png" alt="Illustration"/>
<div style="border-right: 490px solid white">
<p>Kautz graphs are regular graphs used for processor connection patterns
in high-performance and fault-tolerant computing (see its
<a href="http://en.wikipedia.org/wiki/Kautz_graph">Wikipedia entry</a>
for details). <b>igraph</b> is able to generate Kautz graphs (and their
counterparts, <a href="http://en.wikipedia.org/wiki/De_Bruijn_graph">De
Bruijn graphs</a>). Here we used the Python interface to visualize a
Kautz graph with M=3 and N=2 along with its adjacency matrix to decipher
the inner structure of the graph. The adjacency matrix is shown as an
inset in the upper right corner with opacity 0.7.</p>

<pre class="programlisting condensed">
from igraph import *

g = Graph.Kautz(m=3, n=2)
adj = g.get_adjacency()
fig = Plot(bbox=(480, 480))
fig.add(g, layout="fr", vertex_label=None)
fig.add(adj, bbox=(360, 0, 480, 120), grid_width=0, opacity=0.7)
fig.show()
</pre>
</div>

<hr style="clear:both"/>

<a name="6"></a>

<h3 style="clear:both">Minimum spanning tree of a geometric random graph</h3>

<img class="float_right" width="480" height="480" src="images/screenshots/mst.png" alt="Illustration"/>
<div style="border-right: 490px solid white">
<p>The image on the right shows a geometric random graph with 100
vertices dropped randomly onto the unit squre. Two vertices are
connected if and only if their distance is less than 0.2. The edge
weights correspond to the distance of the two endpoints, while the
widths of the edges are proportional to the closeness of the endpoints
(the closer they are, the thicker the edge is). Red edges show the
calculated minimum weight spanning tree of the graph.</p>

<pre class="programlisting condensed">
from igraph import *

def distance(p1, p2):
    return ((p1[0]-p2[0]) ** 2 + (p1[1]-p2[1]) ** 2) ** 0.5
    
g, xs, ys = Graph.GRG(100, 0.2, return_coordinates=True)
layout = Layout(zip(xs, ys))

weights = [distance(layout[edge.source], layout[edge.target]) for edge in g.es]
max_weight = max(weights)
g.es["width"] = [6 - 5*weight/max_weight for weight in weights]
mst = g.spanning_tree(weights)

fig = Plot()
fig.add(g, layout=layout, opacity=0.25, vertex_label=None)
fig.add(mst, layout=layout, edge_color="red", vertex_label=None)
fig.show()
</pre>
</div>

<hr style="clear:both"/>

<a name="7"></a>

<h3>Plotting the diameter</h3>

<img class="float_right" width="503" height="506" alt="Illustration" src="images/screenshots/diameter2.png"/>

<div style="border-right: 513px solid white">
<p>This example shows how vertices along a path (the diameter in this case)
can be selected. We use the igraph R package.</p>
<pre class="programlisting condensed">
library(igraph)

g &lt;- read.graph("karate.net", format="pajek")
d &lt;- get.diameter(g)

E(g)$color &lt;- "grey"
E(g)$width &lt;- 1
E(g, path=d)$color &lt;- "red"
E(g, path=d)$width &lt;- 2
V(g)$label.color &lt;- "blue"
V(g)$color  &lt;- "SkyBlue2"
V(g)[ d ]$label.color &lt;- "black"
V(g)[ d ]$color &lt;- "red"

plot(g, layout=layout.fruchterman.reingold, 
     vertex.label.dist=0, vertex.size=15)
title(main="Diameter of the Zachary Karate Club network",
      xlab="created by igraph 0.4")
axis(1, labels=FALSE, tick=TRUE)
axis(2, labels=FALSE, tick=TRUE)
</pre>
<p>You can find the <a href="karate.net">karate.net file here.</a></p>
</div>

<hr style="clear:both"/>

<a name="8"></a>

<h3>Degree distributions for nonlinear preferential attachment</h3>

<img class="float_right" width="500" height="500" alt="Illustration" src="images/screenshots/degreedist.png"/>
<div style="border-right: 510px solid white">
<p>We plot how the (cumulative) in-degree distribution of a graph changes
if we change the exponent of the preferential attachment process.
We use the igraph R package.</p>
<pre class="programlisting condensed">
library(igraph)

g &lt;- barabasi.game(100000)
d &lt;- degree(g, mode="in")
dd &lt;- degree.distribution(g, mode="in", cumulative=TRUE)
alpha &lt;- power.law.fit(d, xmin=20)
plot(dd, log="xy", xlab="degree", ylab="cumulative frequency",
     col=1, main="Nonlinear preferential attachment")
lines(10:500, 10*(10:500)^(-coef(alpha)+1))

powers &lt;- c(0.9, 0.8, 0.7, 0.6)
for (p in seq(powers)) {
  g &lt;- barabasi.game(100000, power=powers[p])
  dd &lt;- degree.distribution(g, mode="in", cumulative=TRUE)
  points(dd, col=p+1, pch=p+1)
}

legend(1, 1e-5, c(1,powers), col=1:5, pch=1:5, ncol=1, yjust=0, lty=0)
</pre>
</div>

<hr style="clear:both"/>

<a name="9"></a>

<h3>Plotting the dendrogram of a community structure algorithm</h3>
<img class="float_right" width="500" height="500" alt="Illustration" src="images/screenshots/dendrogram.png"/>
<div style="border-right: 510px solid white">
<p>We use the R package and convert the output of the Walktrap
community finding algorithm to a dendrogram.</p>
<pre class="programlisting condensed">
library(igraph)
g &lt;- read.graph("karate.net", format="pajek")

wt &lt;- walktrap.community(g, modularity=TRUE)
dend &lt;- as.dendrogram(wt, use.modularity=TRUE)
plot(dend, nodePar=list(pch=c(NA, 20)))

</pre>
<p>You can find the <a href="karate.net">karate.net file here.</a></p>
</div>

<hr style="clear:both"/>

<a name="10"></a>

<h3>Creating simple animations</h3>
</td>
<img class="float_right" width="480" height="480" alt="Illustration" src="images/screenshots/eb.gif">
<div style="border-right: 490px solid white">
<p>We plot the network after each iteration of the Girvan-Newman edge
betweenness based community structure finding algorithm.
The edge betweenness values are shown on the left, the top five values 
are also shown as edge colors. The components are coded with 
different vertex colors and the modularity score of the actual division is
on the top.</p>
<p>You can get the <a href="karate.net">karate.net file from here.</a>
Download the animation: 
<a href="images/screenshots/eb.avi">AVI file</a>, 
<a href="images/screenshots/eb.mng">MNG file.</a>
</p>
<pre class="programlisting condensed">
library(igraph) ;library(Cairo)

g &lt;- read.graph("karate.net", format="pajek")
l &lt;- layout.kamada.kawai(g, niter=1000)
ebc &lt;- edge.betweenness.community(g)

colbar &lt;- rainbow(6)
colbar2 &lt;- c(rainbow(5), rep("black",15))

for (i in 1:20) {

  g2 &lt;- delete.edges(g, ebc$removed.edges[seq(length=i-1)])
  
  eb &lt;- edge.betweenness(g2)
  cl &lt;- clusters(g2)$membership
  q &lt;- modularity(g, cl)

  E(g2)$color &lt;- "grey"
  E(g2)[ order(eb, decreasing=TRUE)[1:5]-1 ]$color &lt;- colbar2[1:5]

  E(g2)$width &lt;- 1
  E(g2)[ color != "grey" ]$width &lt;- 2

  # CairoPNG(sprintf("eb-community-%04d.png", i))
  plot(g2, layout=l, vertex.size=6, vertex.label=NA,
       edge.label.color="red", vertex.color=colbar[cl+2],
       edge.label.font=2)
  title(main=paste("Q=", round(q,3)), font=2)
  ty &lt;- seq(1,by=-strheight("1")*1.5, length=20)
  text(-1.3, ty, adj=c(0,0.5), round(sort(eb, dec=TRUE)[1:20],2), col=colbar2,
       font=2)

  # dev.off()
  scan()
}
</pre>
</div>
