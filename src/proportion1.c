/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* We compute:
 *
 * - The proportion of temporal groups that have a point of split of merge.
 * - The proportion of communities that are source of split or destination of
 *   merge.
 *
 * If a node has multiple out-going edges, then we randomly choose to
 * travel along exactly one of those edges.
 */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <assert.h>
#include <igraph.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>  /* timeval */

int count_source_sink(const igraph_t *G,
                      igraph_vector_t *IND,
                      igraph_vector_t *OUTD);
void density_exchange_communities(const int id,
                                  const igraph_t *G,
                                  igraph_vector_t *IND,
                                  igraph_vector_t *OUTD,
                                  FILE *f);
void density_exchange_dycom(const int id,
                            const int N,
                            const int NSM,
                            FILE *f);
void trace_community(const igraph_t *G,
                     igraph_vector_t *IND,
                     igraph_vector_t *OUTD,
                     const igraph_integer_t s,
                     int *N,
                     int *NSM,
                     FILE *fdegree);
void trace_dynamic_communities(const int id,
                               const igraph_t *G,
                               igraph_vector_t *IND,
                               igraph_vector_t *OUTD,
                               int *N,
                               int *NSM,
                               const char *outdir);

/* Count the number of nodes that act as source of community split or
 * sink of community merge.  Let v be a vertex in a digraph G, where G is
 * assumed to be a network derived from community matching.  If the
 * out-degree of v is > 1, then v is a source for community split.  If the
 * in-degree of v is > 1, then v is a sink for community merge.
 *
 * INPUT:
 *
 * - G -- a digraph representing the evolution of all dynamic communities.
 * - IND -- the sequence of in-degrees of G.  We assume that IND[i] is the
 *   in-degree of node i.
 * - OUTD -- the sequence of out-degrees of G.  We assume that OUTD[i] is the
 *   out-degree of node i.
 */
int count_source_sink(const igraph_t *G,
                      igraph_vector_t *IND,
                      igraph_vector_t *OUTD) {
  int i;
  int nss;  /* number of nodes that are source or sink */

  nss = 0;
  for (i = 0; i < (int)igraph_vcount(G); i++) {
    if ((VECTOR(*OUTD)[i] > 1)
        || (VECTOR(*IND)[i] > 1)) {
      nss++;
    }
  }
  return nss;
}

/* The density of communities that act as source of split or destination
 * for merge.
 *
 * INPUT:
 *
 * - id -- the ID of the sampled network.  For each parameter setting, we
 *   sampled a number of synthetic networks from a population of networks.
 * - G -- a digraph representing the evolution of all dynamic communities.
 * - IND -- the sequence of in-degrees of G.  We assume that IND[i] is the
 *   in-degree of node i.
 * - OUTD -- the sequence of out-degrees of G.  We assume that OUTD[i] is the
 *   out-degree of node i.
 * - f -- append results to this file.
 */
void density_exchange_communities(const int id,
                                  const igraph_t *G,
                                  igraph_vector_t *IND,
                                  igraph_vector_t *OUTD,
                                  FILE *f) {
  double density;
  int nnode;
  int nss;

  nss = count_source_sink(G, IND, OUTD);
  nnode = (int)igraph_vcount(G);
  /* Write the density to the given file.  We do not overwrite the file, if */
  /* it exists.  Rather, we append our results to the file. */
  density = (double)nss / (double)nnode;
  fprintf(f, "%i,%i,%i,%.15lf\n", id, nss, nnode, density);
}

/* The density of dynamic communities that have participated in a community
 * split or merge.  In other words, we compute the density of dynamic
 * communities that have nodes that act as source of split or destination
 * of merge.
 *
 * INPUT:
 *
 * - id -- the ID of the sampled network.  For each parameter setting, we
 *   sampled a number of synthetic networks from a population of networks.
 * - N -- the number of dynamic communities.
 * - NSM -- how many dynamic communities act as source of split or sink for
 *   merge.
 * - f -- append results to this file.
 */
void density_exchange_dycom(const int id,
                            const int N,
                            const int NSM,
                            FILE *f) {
  double density;

  /* Write the density to the given file.  We do not overwrite the file, if */
  /* it exists.  Rather, we append our results to the file. */
  density = (double)NSM / (double)N;
  fprintf(f, "%i,%i,%i,%.15lf\n", id, NSM, N, density);
}

/* Sample one dynamic community with the given start node.  If a node has
 * multiple out-going edges, we randomly choose to follow exactly one of these
 * out-going edges.
 *
 * INPUT:
 *
 * - G -- a digraph representation of community matches.
 * - IND -- the sequence of in-degrees of G.  We assume that IND[i] is the
 *   in-degree of node i.
 * - OUTD -- the sequence of out-degrees of G.  We assume that D[i] is the
 *   out-degree of node i.
 * - s -- the node from which to start tracing dynamic communities.  This
 *   must be a node in the given digraph.  We assume that this node has
 *   in-degree zero.  We call this the source node.
 * - N -- the number of dynamic communities.
 * - NSM -- how many dynamic communities act as source of split or sink for
 *   merge.
 * - fdegree -- write to this file the sequences of in-, out-, and total
 *   degrees.
 */
void trace_community(const igraph_t *G,
                     igraph_vector_t *IND,
                     igraph_vector_t *OUTD,
                     const igraph_integer_t s,
                     int *N,
                     int *NSM,
                     FILE *fdegree) {
  int i;
  int j;
  int k;
  int indeg;            /* the in-degree */
  int nmerge;           /* the number of merge points */
  int outdeg;           /* the out-degree */
  int nsplit;           /* the number of split points */
  int u;
  int v;
  igraph_stack_t S;     /* index all those in D to be further explored */
  igraph_vector_t Adj;  /* the neighbours of a node */
  igraph_vector_t T;    /* the trace of a dynamic community */
  unsigned int seed;
  struct timeval tv;

  /* It is possible that s is isolated, i.e. it has one time step.  In this */
  /* case, its in- and out-degrees are both zero. */
  if (VECTOR(*OUTD)[s] == 0) {
    *N += 1;
    /* The file format is "lifespan,degree". */
    fprintf(fdegree, "1,0\n");
    return;
  }

  /* setup the random number generator */
  gettimeofday(&tv, 0);
  seed = tv.tv_sec + tv.tv_usec;
  igraph_rng_seed(igraph_rng_default(), seed);

  /* From hereon, we assume that s has out-degree > 0.  Do a depth-first */
  /* search from s to sample a dynamic community that starts from s.  We */
  /* assume that the given graph is directed and we follow edges that go out */
  /* from s.  We only record nodes that can be reached from s. */
  igraph_vector_init(&T, 0);
  igraph_stack_init(&S, 0);
  igraph_stack_push(&S, (int)s);
  while (igraph_stack_size(&S) > 0) {
    u = (int)igraph_stack_pop(&S);
    igraph_vector_push_back(&T, u);
    /* record all successors that have exactly one out-neighbour */
    i = (int)igraph_vector_size(&T) - 1;
    v = (int)VECTOR(T)[i];
    while (VECTOR(*OUTD)[v] == 1) {
      igraph_vector_init(&Adj, 0);
      igraph_neighbors(G, &Adj, v, IGRAPH_OUT);
      igraph_vector_push_back(&T, (int)VECTOR(Adj)[0]);
      i = (int)igraph_vector_size(&T) - 1;
      v = (int)VECTOR(T)[i];
      igraph_vector_destroy(&Adj);
    }
    /* If a node has > 1 out-neighbour, choose uniformly at random to */
    /* follow one of these out-neighbours.  Then push the chosen neighbours */
    /* onto the stack.  If we don't follow an out-neighbour, we won't be */
    /* able to reach the end point of a dynamic community. */
    i = (int)igraph_vector_size(&T) - 1;
    v = (int)VECTOR(T)[i];
    if (VECTOR(*OUTD)[v] > 1) {
      igraph_vector_init(&Adj, 0);
      igraph_neighbors(G, &Adj, v, IGRAPH_OUT);
      j = (int)igraph_rng_get_integer(igraph_rng_default(),
                                      0,                   /*lower limit*/
                                      VECTOR(*OUTD)[v] - 1 /*upper limit*/);
      igraph_stack_push(&S, (int)VECTOR(Adj)[j]);
      igraph_vector_destroy(&Adj);
      continue;
    }
    /* We have reached the end point of a dynamic community. */
    i = (int)igraph_vector_size(&T) - 1;
    v = (int)VECTOR(T)[i];
    if (VECTOR(*OUTD)[v] == 0) {
      /* We have found another dynamic community.  Is this dynamic community */
      /* also a source of split or a destination of merge? */
      *N += 1;
      for (k = 0; k < (int)igraph_vector_size(&T); k++) {
        v = (int)VECTOR(T)[k];
        if ((VECTOR(*OUTD)[v] > 1)
            || (VECTOR(*IND)[v] > 1)) {
          *NSM += 1;
          break;
        }
      }

      /* Get the total degree of this dynamic community.  The total degree */
      /* is the sum of the in- and out-degrees.  The in-degree is the */
      /* number of outside edges that point to this dynamic community.  Let */
      /* u be a snapshot of a dynamic community.  If u is a point of merge, */
      /* then one edge from the dynamic community is pointing to u and the */
      /* remaining edges are outside edges as these emanate from other */
      /* dynamic communities.  Let v_1, ..., v_k be all snapshots of the */
      /* dynamic community, each of which is a point of merge.  If */
      /* indeg(v_i) is the in-degree of v_i, then the in-degree of the */
      /* dynamic community is given by (\sum_{i=1}^k indeg(v_i)) - k. */
      /* The out-degree is the number of edges that go outside of this */
      /* dynamic community.  Let u be a snapshot of a dynamic community.  If */
      /* u is a point of split, then one edge from the dynamic community is */
      /* pointing to the next snapshot while the remaining edges are */
      /* pointing outside to other dynamic communities.  Let v_1, ..., v_k */
      /* be all snapshots of the dynamic community, each of which is a point */
      /* of split.  If outdeg(v_i) is the out-degree of v_i, then the */
      /* out-degree of the dynamic community is given by */
      /* (\sum_{i=1}^k outdeg(v_i)) - k. */
      indeg = 0;   /* in-degree of the dynamic community */
      nmerge = 0;  /* number of merge points */
      outdeg = 0;  /* out-degree of the dynamic community */
      nsplit = 0;  /* number of merge points */
      for (k = 0; k < (int)igraph_vector_size(&T); k++) {
        v = (int)VECTOR(T)[k];
        if (VECTOR(*IND)[v] > 1) {
          indeg += (int)VECTOR(*IND)[v];
          nmerge++;
        }
        if (VECTOR(*OUTD)[v] > 1) {
          outdeg += (int)VECTOR(*OUTD)[v];
          nsplit++;
        }
      }
      indeg = indeg - nmerge;
      outdeg = outdeg - nsplit;
      /* The file format is "lifespan,degree". */
      fprintf(fdegree, "%i,%i\n", (int)igraph_vector_size(&T), indeg + outdeg);

      /* clear the stack */
      if (igraph_stack_size(&S) > 0) {
        igraph_stack_clear(&S);
      }
      assert(igraph_stack_size(&S) == 0);
    }
  }
  igraph_stack_destroy(&S);
  igraph_vector_destroy(&T);
}

/* Trace all dynamic communities using the given digraph.  Each community
 * is traced from its birth to its death, or possibly up to the latest
 * snapshot for which we have data.
 *
 * INPUT:
 *
 * - id -- the ID of the sampled network.
 * - G -- a digraph representation of community matches.  We use this
 *   digraph representation to trace the evolution of each dynamic
 *   community.
 * - IND -- the sequence of in-degrees of G.  We assume that IND[i] is the
 *   in-degree of node i.
 * - OUTD -- the sequence of out-degrees of G.  We assume that OUTD[i] is the
 *   out-degree of node i.
 * - N -- the number of dynamic communities.
 * - NSM -- how many dynamic communities act as source of split or sink for
 *   merge.
 * - outdir -- write results to files under this directory.
 */
void trace_dynamic_communities(const int id,
                               const igraph_t *G,
                               igraph_vector_t *IND,
                               igraph_vector_t *OUTD,
                               int *N,
                               int *NSM,
                               const char *outdir) {
  FILE *fdegree;
  char *fname;
  int i;

  asprintf(&fname, "%s/elldeg/%i.dat", outdir, id);
  fdegree = fopen(fname, "w");  /* overwrite if it exists */
  fclose(fdegree);
  fdegree = fopen(fname, "a");
  free(fname);
  for (i = 0; i < (int)igraph_vector_size(IND); i++) {
    /* Get the source node of a dynamic community.  This node is the */
    /* very first node in the history of the dynamic community.  A source */
    /* node is defined as any node whose in-degree is zero.  Sample one */
    /* dynamic community that starts from the given source node. */
    if (VECTOR(*IND)[i] == 0) {
      trace_community(G, IND, OUTD, i, N, NSM, fdegree);
    }
  }
  fclose(fdegree);
}

/* Density of interaction among dynamic communities.
 */
int main(int argc,
         char **argv) {
  FILE *f;
  char *fname;
  int n;
  int N;               /* number of dynamic communities */
  int NSM;             /* #dycoms that have participated in a split or merge */
  igraph_t G;             /* the sampled network; must be directed & simple */
  igraph_vector_t IND;    /* sequence of in-degree */
  igraph_vector_t OUTD;   /* sequence of out-degree */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_str *datadir = arg_strn(NULL, "datadir", "path", 0, argc + 2,
                                     "directory with sampled networks");
  struct arg_str *sampleid = arg_strn(NULL, "sampleid", "integer", 0, argc + 2,
                                      "the sample ID");
  struct arg_str *outdir = arg_strn(NULL, "outdir", "path", 0, argc + 2,
                                "write results to files under this directory");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, datadir, sampleid, outdir, end};
  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Density of interaction among dynamic communities.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((datadir->count < 1)
      || (sampleid->count < 1)
      || (outdir->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Density of interaction among dynamic communities.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  /* process the given sampled network */
  n = atoi(sampleid->sval[0]);
  /* Construct the sampled graph from its edge list.  The graph is */
  /* a simple digraph, i.e. no self-loops nor multiple edges. */
  asprintf(&fname, "%s/%i.dat", datadir->sval[0], n);
  f = fopen(fname, "r");
  igraph_read_graph_edgelist(&G, f, 0, IGRAPH_DIRECTED);
  fclose(f);
  free(fname);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);
  /* get all traces of dynamic communities */
  igraph_vector_init(&OUTD, 0);
  igraph_degree(&G, &OUTD, igraph_vss_all(), IGRAPH_OUT, IGRAPH_NO_LOOPS);
  igraph_vector_init(&IND, 0);
  igraph_degree(&G, &IND, igraph_vss_all(), IGRAPH_IN, IGRAPH_NO_LOOPS);
  N = 0;
  NSM = 0;
  trace_dynamic_communities(n, &G, &IND, &OUTD, &N, &NSM, outdir->sval[0]);
  /* compute densities and write results to file */
  asprintf(&fname, "%s/density-nss.dat", outdir->sval[0]);
  f = fopen(fname, "a");
  density_exchange_communities(n, &G, &IND, &OUTD, f);
  fclose(f);
  free(fname);
  igraph_destroy(&G);
  asprintf(&fname, "%s/density-dycom.dat", outdir->sval[0]);
  f = fopen(fname, "a");
  density_exchange_dycom(n, N, NSM, f);
  fclose(f);
  free(fname);

  igraph_vector_destroy(&IND);
  igraph_vector_destroy(&OUTD);
  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

  return 0;
}
