# Setup for building C extensions.

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

myext = ""  # change me
ext_modules = [Extension(myext, ["%s.pyx" % myext])]

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)
