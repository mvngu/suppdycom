/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* This program generates a synthetic trace network.  The lifespan of each
 * temporal group is randomly drawn from a power-law distribution.
 */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <assert.h>
#include <ctype.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <igraph.h>
#include <libcalg.h>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>  /* timeval */

void add_new_node(const int index,
                  Set *Node,
                  Trie *Vt,
                  igraph_strvector_t *Vtlabel,
                  const int t,
                  const double alpha);
void bipartite_network(const int t,
                       Set *Node,
                       FILE *f,
                       Trie *V,
                       igraph_strvector_t *label,
                       Trie *Vs,
                       igraph_strvector_t *Vslabel,
                       Trie *Vt,
                       igraph_strvector_t *Vtlabel);
void build_dycom_network(const int niter,
                         const double alpha,
                         const double prob,
                         const int ncom,
                         const char *outfile);
void copy_node_set(Trie *V,
                   igraph_strvector_t *Vlabel,
                   Trie *W,
                   igraph_strvector_t *Wlabel);
void init_pool(const int ncom,
               const double alpha,
               Set *Node,
               Trie *V,
               igraph_strvector_t *label);
void random_merge(igraph_strvector_t *Vslabel,
                  igraph_strvector_t *Vtlabel,
                  Set *Node,
                  FILE *f,
                  const double p);
void random_split(igraph_strvector_t *Vslabel,
                  igraph_strvector_t *Vtlabel,
                  Set *Node,
                  FILE *f,
                  const double p);

/* Construct a new node for the given time step.
 *
 * WARNING: This function modifies its arguments.
 *
 * INPUT:
 *
 * - index -- the index to use for the new node.
 * - Node -- set of nodes.  We will update this as we go along.
 * - Vt -- the set of step communities.  The new node will be added here.
 * - Vtlabel -- the labels of the nodes in Vt.
 * - t -- current time step.
 * - alpha -- parameter for the power law distribution.  We use the power law
 *   distribution p(x) \sim x^{-\alpha} to model the lifespan of each dynamic
 *   community.
 */
void add_new_node(const int index,
                  Set *Node,
                  Trie *Vt,
                  igraph_strvector_t *Vtlabel,
                  const int t,
                  const double alpha) {
  int age;
  int lifespan;
  char *key;
  char *val;
  char *u;
  double gamma;
  double r;  /* random number uniformly distributed in [0,1) */
  gsl_rng *rng;
  unsigned int seed;
  struct timeval tv;

  age = 1;
  gamma = 1.0 / (1 - alpha);
  /* setup random number generator */
  gettimeofday(&tv, 0);
  seed = tv.tv_sec + tv.tv_usec;
  rng = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_rng_set(rng, seed);
  /* generate and add a new node */
  lifespan = 0;
  while (lifespan < 1) {
    r = gsl_rng_uniform(rng);
    lifespan = (int)ceil(pow(r, gamma));
  }
  assert(lifespan > 0);
  asprintf(&u, "%d,%d", index, t);                        /* a node */
  assert(set_insert(Node, u) != 0);                       /* global node set */
  asprintf(&key, "%d,%d", index, t);                      /* a node */
  asprintf(&val, "%d,%d,%d,%d", index, t, lifespan, age); /* its full data */
  assert(trie_insert(Vt, key, val) != 0);                 /* initial pool */
  igraph_strvector_add(Vtlabel, key);                     /* maintain label */
  gsl_rng_free(rng);
}

/* From the given set of front communities, generate a preliminary set of
 * step communities.  Together, these two sets of nodes form a bipartite
 * network.  We also update the edge set as we go along.
 *
 * WARNING: This function modifies its arguments.
 *
 * INPUT:
 *
 * - t -- current time step.
 * - Node -- the running set of vertices; the set of all nodes.  As we go
 *   along, ideally this set should maintain only isolated nodes.
 * - f -- write edges to this file.
 * - V -- set of front communities at time t.
 * - label -- labels for the communities in V.
 * - Vs -- the updated set of front communities at time t, once we have
 *   removed communities that have lived out their lifespans.  This should be
 *   initialized beforehand.
 * - Vslabel -- labels for the front communities.  This should be initialized
 *   beforehand and will be updated as required.
 * - Vt -- the set of step communities at time t.  This should be initialized
 *   beforehand and will be updated as required.
 * - Vtlabel -- labels for the step communities.  This should be initialized
 *   beforehand and will be updated as required.
 */
void bipartite_network(const int t,
                       Set *Node,
                       FILE *f,
                       Trie *V,
                       igraph_strvector_t *label,
                       Trie *Vs,
                       igraph_strvector_t *Vslabel,
                       Trie *Vt,
                       igraph_strvector_t *Vtlabel) {
  int index;
  int timestep;
  int lifespan;
  int age;
  int i;
  char *key;
  char *val;
  char *Vskey;
  char *Vsval;
  char *Vtkey;
  char *Vtval;
  char *edge;  /* a directed edge */
  char *vert;

  assert(trie_num_entries(V) == (int)igraph_strvector_size(label));
  for (i = 0; i < (int)igraph_strvector_size(label); i++) {
    key = STR(*label, i);
    val = (char *)trie_lookup(V, key);
    assert(sscanf(val, "%d,%d,%d,%d", &index, &timestep, &lifespan, &age)
           == 4);
    if (age == lifespan) {
      continue;
    }
    assert(age < lifespan);
    assert(t == timestep + 1);
    /* update the set of front communities */
    asprintf(&Vskey, "%d,%d", index, timestep);
    asprintf(&Vsval, "%d,%d,%d,%d", index, timestep, lifespan, age);
    assert(trie_insert(Vs, Vskey, Vsval) != 0);
    igraph_strvector_add(Vslabel, Vskey);
    /* the set of step communities */
    asprintf(&Vtkey, "%d,%d", index, t);
    asprintf(&Vtval, "%d,%d,%d,%d", index, t, lifespan, age + 1);
    assert(trie_insert(Vt, Vtkey, Vtval) != 0);
    igraph_strvector_add(Vtlabel, Vtkey);
    /* write the edge to file */
    asprintf(&edge, "%d,%d %d,%d", index, timestep, index, t);
    fprintf(f, "%s\n", edge);
    free(edge);
    /* maintain the set of isolated nodes */
    asprintf(&vert, "%d,%d", index, timestep);
    if (set_query(Node, vert)) {
      set_remove(Node, vert);
    }
    free(vert);
    asprintf(&vert, "%d,%d", index, t);
    if (set_query(Node, vert)) {
      set_remove(Node, vert);
    }
    free(vert);
  }
}

/* Generate a network to represent the evolution of interaction among
 * dynamic communities.
 *
 * INPUT:
 *
 * - niter -- number of iterations, also the number of time steps.
 * - alpha -- parameter for the power law distribution.  We use the power law
 *   distribution p(x) \sim x^{-\alpha} to model the lifespan of each dynamic
 *   community.
 * - prob -- probability of merge or split.
 * - ncom -- initial number of step communities.  We treat each step
 *   community as a node.
 * - outfile -- write the edge list to this file.
 */
void build_dycom_network(const int niter,
                         const double alpha,
                         const double prob,
                         const int ncom,
                         const char *outfile) {
  FILE *f;
  int n;      /* how many nodes in all */
  int t;      /* time step */
  Set *Node;  /* the set of vertices */
  SetIterator itr;
  Trie *V;    /* a set of front communities */
  Trie *Vs;   /* set of front communities, i.e. nodes */
  Trie *Vt;   /* set of step communities, i.e. nodes */
  igraph_strvector_t label;  /* labels for front communities */
  igraph_strvector_t Vslabel;
  igraph_strvector_t Vtlabel;
  char *vert;

  Node = set_new(string_hash, string_equal);
  set_register_free_function(Node, free);
  V = trie_new();
  igraph_strvector_init(&label, 0);
  init_pool(ncom, alpha, Node, V, &label);
  n = ncom;

  /* construct network of dynamic communities */
  /* write edges to file as we go along */
  f = fopen(outfile, "w");
  for (t = 1; t < niter; t++) {
    /* Generate directed bipartite network between two sets of nodes */
    /* in consecutive time steps. */
    Vs = trie_new();
    igraph_strvector_init(&Vslabel, 0);
    Vt = trie_new();
    igraph_strvector_init(&Vtlabel, 0);
    bipartite_network(t, Node, f, V, &label, Vs, &Vslabel, Vt, &Vtlabel);
    add_new_node(n, Node, Vt, &Vtlabel, t, alpha);
    n++;

    random_split(&Vslabel, &Vtlabel, Node, f, prob);
    random_merge(&Vslabel, &Vtlabel, Node, f, prob);
    /* update the sets of front and step communities */
    trie_free(V);
    igraph_strvector_destroy(&label);
    V = trie_new();
    igraph_strvector_init(&label, 0);
    copy_node_set(Vt, &Vtlabel, V, &label);  /* V is now front for time t+1 */
    trie_free(Vs);
    igraph_strvector_destroy(&Vslabel);
    trie_free(Vt);
    igraph_strvector_destroy(&Vtlabel);
  }

  /* Now write all the isolated nodes.  We treat each isolated node as a */
  /* self-loop.  However, when we read in the resulting edge list, we should */
  /* remove all self-loops.  That way, we would be able to recover */
  /* isolated nodes. */
  set_iterate(Node, &itr);
  while (set_iter_has_more(&itr)) {
    vert = (char *)set_iter_next(&itr);
    fprintf(f, "%s %s\n", vert, vert);
  }
  fclose(f);

  set_free(Node);
  trie_free(V);
  igraph_strvector_destroy(&label);
}

/* Copy nodes in V into the new node set W.
 *
 * INPUT:
 *
 * - V -- a set of nodes whose elements we want to copy.
 * - Vlabel -- the labels of all nodes in V.
 * - W -- a node set that is a copy of V.  This is should be initialized
 *   beforehand.
 * - Wlabel -- the labels of all nodes in W.
 */
void copy_node_set(Trie *V,
                   igraph_strvector_t *Vlabel,
                   Trie *W,
                   igraph_strvector_t *Wlabel) {
  int i;
  int index;
  int timestep;
  int lifespan;
  int age;
  char *key;
  char *val;
  char *cpkey;
  char *cpval;

  assert(trie_num_entries(V) == (int)igraph_strvector_size(Vlabel));
  for (i = 0; i < (int)igraph_strvector_size(Vlabel); i++) {
    key = STR(*Vlabel, i);
    val = (char *)trie_lookup(V, key);
    assert(sscanf(val, "%d,%d,%d,%d", &index, &timestep, &lifespan, &age)
           == 4);
    asprintf(&cpkey, "%d,%d", index, timestep);
    asprintf(&cpval, "%d,%d,%d,%d", index, timestep, lifespan, age);
    assert(trie_insert(W, cpkey, cpval) != 0);
    igraph_strvector_add(Wlabel, cpkey);
  }
}

/* Initialize the pool of nodes.  That is, generate a pool of nodes at
 * time step t = 0.  Each node is assigned a lifespan drawn from an
 * exponential distribution with the given parameter alpha.
 *
 * INPUT:
 *
 * - ncom -- initial number of step communities.  We treat each step
 *   community as a node.
 * - alpha -- parameter for the power law distribution.  We use the power law
 *   distribution p(x) \sim x^{-\alpha} to model the lifespan of each dynamic
 *   community.
 * - Node -- the set of all nodes; we update this as we go along.  This is not
 *   necessarily the same as the global list Node of nodes.  This should be
 *   initialized beforehand.
 * - V -- the pool of initial nodes.  This should be initialized beforehand.
 * - label -- list of labels for front communities.  This should be
 *   initialized beforehand and will be updated as necessary.
 */
void init_pool(const int ncom,
               const double alpha,
               Set *Node,
               Trie *V,
               igraph_strvector_t *label) {
  int age;  /* how many time steps has this node appeared i */
  int t;    /* time step */
  int i;
  int ell;  /* lifespan */
  double gamma;
  double r;  /* random number uniformly distributed in [0,1) */
  char *key;
  char *val;
  char *u;  /* a node */
  gsl_rng *rng;
  unsigned int seed;
  struct timeval tv;

  age = 1;
  t = 0;
  gamma = 1.0 / (1 - alpha);
  /* setup random number generator */
  gettimeofday(&tv, 0);
  seed = tv.tv_sec + tv.tv_usec;
  rng = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_rng_set(rng, seed);
  /* get initial pool of nodes */
  for (i = 0; i < ncom; i++) {
    ell = 0;
    while (ell < 1) {
      r = gsl_rng_uniform(rng);
      ell = (int)ceil(pow(r, gamma));               /* lifespan */
    }
    assert(ell > 0);
    asprintf(&u, "%d,%d", i, t);                    /* a node */
    assert(set_insert(Node, u) != 0);               /* global node set */
    asprintf(&key, "%d,%d", i, t);                  /* a node */
    asprintf(&val, "%d,%d,%d,%d", i, t, ell, age);  /* its full data */
    assert(trie_insert(V, key, val) != 0);          /* initial pool */
    igraph_strvector_add(label, key);               /* maintain label */
  }
  gsl_rng_free(rng);
}

/* Create a number of random merge events.  For each node v in Vt, we draw a
 * probability q uniformly at random from (0,1).  If q < p, then we choose
 * a node u uniformly at random from Vs.  The node u is connected to v.
 * This is the merge step.
 *
 * WARNING: This function modifies its arguments.
 *
 * INPUT:
 *
 * - Vslabel -- the labels for nodes in Vs.
 * - Vtlabel -- the labels for nodes in Vt.
 * - Node -- the running set of vertices; the set of all nodes.  As we go
 *   along, ideally this set should maintain only isolated nodes.
 * - f -- write edges to this file.
 * - p -- probability of split.
 */
void random_merge(igraph_strvector_t *Vslabel,
                  igraph_strvector_t *Vtlabel,
                  Set *Node,
                  FILE *f,
                  const double p) {
  double q;
  int i;
  int j;
  char *edge;
  char *vert;
  unsigned int seed;
  struct timeval tv;

  /* We can't sample a node if the population has no nodes at all. */
  if (igraph_strvector_size(Vslabel) < 1) {
    return;
  }

  /* setup the random number generator */
  gettimeofday(&tv, 0);
  seed = tv.tv_sec + tv.tv_usec;
  igraph_rng_seed(igraph_rng_default(), seed);

  /* generate random merge */
  for (i = 0; i < (int)igraph_strvector_size(Vtlabel); i++) {
    q = (double)igraph_rng_get_unif01(igraph_rng_default());
    if (q < p) {
      /* randomly choose a node from Vs */
      j = (int)igraph_rng_get_integer(igraph_rng_default(), 0, /* low */
                                      igraph_strvector_size(Vslabel) - 1);
      /* write edge to file */
      asprintf(&edge, "%s %s", STR(*Vslabel, j), STR(*Vtlabel, i));
      fprintf(f, "%s\n", edge);
      free(edge);
      /* maintain set of isolated nodes */
      asprintf(&vert, "%s", STR(*Vtlabel, i));
      if (set_query(Node, vert)) {
        set_remove(Node, vert);
      }
      free(vert);
      asprintf(&vert, "%s", STR(*Vslabel, j));
      if (set_query(Node, vert)) {
        set_remove(Node, vert);
      }
      free(vert);
    }
  }
}

/* Create a number of random split events.  For each node v in Vs, we draw a
 * probability q uniformly at random from (0,1).  If q < p, then we choose
 * a node u uniformly at random from Vt.  The node v is connected to u.
 * This is the split step.
 *
 * WARNING: This function modifies its arguments.
 *
 * INPUT:
 *
 * - Vslabel -- the labels for nodes in Vs.
 * - Vtlabel -- the labels for nodes in Vt.
 * - Node -- the running set of vertices; the set of all nodes.  As we go
 *   along, ideally this set should maintain only isolated nodes.
 * - f -- write edges to this file.
 * - p -- probability of split.
 */
void random_split(igraph_strvector_t *Vslabel,
                  igraph_strvector_t *Vtlabel,
                  Set *Node,
                  FILE *f,
                  const double p) {
  double q;
  int i;
  int j;
  char *edge;
  char *vert;
  unsigned int seed;
  struct timeval tv;

  /* We can't sample a node if the population has no nodes at all. */
  if (igraph_strvector_size(Vtlabel) < 1) {
    return;
  }

  /* setup the random number generator */
  gettimeofday(&tv, 0);
  seed = tv.tv_sec + tv.tv_usec;
  igraph_rng_seed(igraph_rng_default(), seed);

  /* generate random split */
  for (i = 0; i < (int)igraph_strvector_size(Vslabel); i++) {
    q = (double)igraph_rng_get_unif01(igraph_rng_default());
    if (q < p) {
      /* randomly choose a node from Vt */
      j = (int)igraph_rng_get_integer(igraph_rng_default(), 0, /* low */
                                      igraph_strvector_size(Vtlabel) - 1);
      /* write edge to file */
      asprintf(&edge, "%s %s", STR(*Vslabel, i), STR(*Vtlabel, j));
      fprintf(f, "%s\n", edge);
      free(edge);
      /* maintain set of isolated nodes */
      asprintf(&vert, "%s", STR(*Vslabel, i));
      if (set_query(Node, vert)) {
        set_remove(Node, vert);
      }
      free(vert);
      asprintf(&vert, "%s", STR(*Vtlabel, j));
      if (set_query(Node, vert)) {
        set_remove(Node, vert);
      }
      free(vert);
    }
  }
}

/* Generate a synthetic dynamic community.
 */
int main(int argc,
         char **argv) {
  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_str *niter = arg_strn(NULL, "niter", "integer", 0, argc + 2,
                        "number of iterations, also the number of time steps");
  struct arg_str *alpha = arg_strn(NULL, "alpha", "float", 0, argc + 2,
                                   "parameter for the power law distribution");
  struct arg_str *p = arg_strn(NULL, "p", "float", 0, argc + 2,
                               "probability of merge or split");
  struct arg_str *ncom = arg_strn(NULL, "ncom", "integer", 0, argc + 2,
                                  "initial number of step communities");
  struct arg_str *outfile = arg_strn(NULL, "outfile", "path", 0, argc + 2,
                                     "write the edge list to this file");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, niter, alpha, p, ncom, outfile, end};
  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Generate a synthetic dynamic community.\n");
    arg_print_glossary(stdout, argtable, " %-17s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((niter->count < 1)
      || (alpha->count < 1)
      || (p->count < 1)
      || (ncom->count < 1)
      || (outfile->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Generate a synthetic dynamic community.\n");
    arg_print_glossary(stdout, argtable, " %-17s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  build_dycom_network(atoi(niter->sval[0]), strtod(alpha->sval[0], NULL),
                      strtod(p->sval[0], NULL), atoi(ncom->sval[0]),
                      outfile->sval[0]);

  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

  return 0;
}
